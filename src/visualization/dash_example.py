import dash
import dash_core_components as dcc
import dash_html_components as html
import pandas as pd
import numpy as np
import plotly.express as px
import src.utilities.heatmap_functions as heat
from dash.dependencies import Input, Output
import json
import os
import dropbox


external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

ts_df = pd.read_csv('../../data/processed/Scerevisiae_alphafactor_245-5min_RNAseq_dropped.tsv',
                    sep='\t', comment='#', index_col=0)
timepoints = ts_df.columns.tolist()
slider_dict = {str(idx): tp for idx, tp in zip(np.arange(0, len(timepoints), 1), timepoints)}
total_genes = ts_df.shape[0]

jtk_df = pd.read_csv('../../data/periodicity_results/node_finding_190411114935/pyjtk_results.tsv',
                    sep='\t', comment='#', index_col=0)

pyjtk_sorted_thres = jtk_df[jtk_df["p-value"] <= 0.9]
filtered_genes = pyjtk_sorted_thres.index.tolist()

pval_histo = px.histogram(jtk_df, x="p-value", hover_name="p-value", hover_data={"p-value":False})
pval_histo.update_layout(hovermode='x unified',
                         xaxis={'showspikes': True,
                                'spikemode': 'across',
                                'spikesnap': 'cursor'})
# pval_histo.update_layout(dragmode="drawrect")
# pval_histo.add_shape(
#         # unfilled Rectangle
#             type="line",
#             x0=0.5,
#             x1=0.5,
#             y0=0,
#             y1=0.9,
#             xref='paper',
#             yref='paper',
#             line=dict(
#                 color="Red",
#             ),
#         )


app.layout = html.Div(children=[
    html.H1(children='Yeast Cell Cycle RNAseq'),
    html.Div(["P-value Threshold: ", dcc.Input(id='thres-input', value='0.9', type='text')]),
    # html.Div(className='row',
    #          style={'display': 'flex'},
    #          children=[
    #                     html.Div(["P-value Threshold: ", dcc.Input(id='thres-input', value='0.9', type='text')]),
    #                     html.Div(f'{len(filtered_genes)} genes, {round((len(filtered_genes)/total_genes)*100, 2)}% of all genes.', id='result')]),
    dcc.Slider(
            id='period-slider',
            min=0,
            max=len(timepoints),
            marks=slider_dict,
            value=3
        ),
    dcc.RangeSlider(
            id='trunc-range-slider',
            min=-0,
            max=len(timepoints),
            marks=slider_dict,
            value=[0, len(timepoints)],
            allowCross=False
        ),
    html.Div(f'{len(filtered_genes)} genes, {round((len(filtered_genes)/total_genes)*100, 2)}% of all genes.', id='pval_info'),
    html.Div(
            [dcc.Graph(id='heatmap', figure=heat.heatmap_max_ploty(ts_df, filtered_genes, 3)),],
            style={"display": "inline-block", "padding": "0 0"},
        ),
    html.Div(
            [dcc.Graph(id='pval_histo', figure=pval_histo, config=dict(editable=True)),],
            style={"width": "40%", "display": "inline-block", "padding": "0 0"},
        ),
    html.Pre(id="annotations-data")
             ])


@app.callback(
    Output(component_id="annotations-data", component_property="children"),
    Input(component_id="pval_histo", component_property="relayoutData"),
    prevent_initial_call=True,
)
def on_new_annotation(relayout_data):
    if "shapes" in relayout_data:
        last_shape = relayout_data["shapes"][-1]
        left_pval, right_pval = last_shape["x0"], last_shape["x1"]
        return left_pval, right_pval
    else:
        return dash.no_update


@app.callback(
    Output(component_id='heatmap', component_property='figure'),
    Input(component_id='thres-input', component_property='value'),
    Input(component_id='period-slider', component_property='value'),
    Input(component_id='trunc-range-slider', component_property='value'),
    Input(component_id="pval_histo", component_property="relayoutData"),
    prevent_initial_call=True
)
def update_heatmap_figure(pval, period, trunc_list, relayout_data):

    if "shapes" in relayout_data:
        last_shape = relayout_data["shapes"][-1]
        left_pval, right_pval = last_shape["x0"], last_shape["x1"]
        filtered_jtk_df = jtk_df[jtk_df['p-value'] <= float(right_pval)]
    else:
        filtered_jtk_df = jtk_df[jtk_df['p-value'] <= float(pval)]

    trunc_ts_df = ts_df.iloc[:, trunc_list[0]:trunc_list[1]]
    heatmap_fig = heat.heatmap_max_ploty(trunc_ts_df, filtered_jtk_df.index.tolist(), period)
    heatmap_fig.update_layout(margin=dict(t=20, b=20))
    heatmap_fig.update_yaxes(showticklabels=False)
    return heatmap_fig

@app.callback(
    Output(component_id='pval_info', component_property='children'),
    Input(component_id='thres-input', component_property='value'))

def update_result(pval):
    filtered_jtk_df = jtk_df[jtk_df['p-value'] <= float(pval)]
    filtered_genes = filtered_jtk_df.index.tolist()
    return f'{len(filtered_genes)} genes, {round((len(filtered_genes)/total_genes)*100, 2)}% of all genes.'





if __name__ == '__main__':
    app.run_server(debug=True)