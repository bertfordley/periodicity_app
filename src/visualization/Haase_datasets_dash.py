import dash
import dash_core_components as dcc
import dash_html_components as html
import pandas as pd
import numpy as np
import src.utilities.heatmap_functions as heat
from dash.dependencies import Input, Output, State
import json
import os
import dropbox
import io
from dash.exceptions import PreventUpdate


external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)


BASE_DIR = '../..'

with open(os.path.join(BASE_DIR, 'secrets.json')) as secrets_file:
    secrets = json.load(secrets_file)
# def get_secret(setting, secrets=secrets):
#     """Get secret setting or fail with ImproperlyConfigured"""
#     try:
#         return secrets[setting]
#     except KeyError:
#         raise ImproperlyConfigured("Set the {} setting".format(setting))
DBX_ACCESSTOKEN = secrets['DBX_ACCESSTOKEN']
dbx = dropbox.Dropbox(oauth2_access_token=DBX_ACCESSTOKEN)

ma_files_path = secrets['DBX_MICROARRAY_DATAPATH']
all_ma_files = dbx.files_list_folder(ma_files_path).entries
cleaned_ma_file_names = [file.name for file in all_ma_files if file.name.endswith('_clean.txt')]
cleaned_ma_file_names = [file for file in cleaned_ma_file_names if not file.endswith('_all_annot_clean.txt')]

# all_rnaseq_files= []
rnaseq_files_path = secrets['DBX_RNASEQ_DATAPATH']
all_rnaseq_files = dbx.files_list_folder(rnaseq_files_path).entries
all_rnaseq_files = [file.name for file in all_rnaseq_files]

all_file_names = all_rnaseq_files + cleaned_ma_file_names

filename_dict = [{'label': filename, 'value': filename} for filename in all_file_names]
# filename_dict = [{'label': filename, 'value': filename} for filename in cleaned_ma_file_names]


# cleaned_file_names = cleaned_file_names[:2]
# cleaned_file_dict = [{'label': '_'.join(filename.split('_')[:-6]), 'value': '_'.join(filename.split('_')[:-6])} for filename in cleaned_file_names]

app.layout = html.Div(children=[
    # Default storage is memory, lost on page reload.
    html.Div("Select the dataset to be displayed:"),
    dcc.Dropdown(
        id='dataset-dropdown',
        options=filename_dict,
        # value=default_pretty_name,
        placeholder="Select a dataset",
        style={'width': '700px'}# 'height': '30px',
    ),
    dcc.Store(id='memory'),
    html.Button('Load Data', id='data-load-button'),
    html.Div(id='period-slider-output'),
    html.Div(dcc.Slider(
            id='period-slider',
            min=0,
            max=50,
            value=3,
            updatemode='drag'),
        style={'width': '30%'}),
    html.Div(id='trunc-range-slider-output'),
    html.Div(dcc.RangeSlider(
            id='trunc-range-slider',
            min=-0,
            max=200,
            value=[0, 200],
            allowCross=False,
            updatemode='drag'),
        style={'width': '30%'}),
    html.Div(children=[], id='heatmap',
             style={"display": "inline-block", "padding": "0 0"},
             ),
])

@app.callback(
    Output(component_id='memory', component_property='data'),
    [Input(component_id='data-load-button', component_property='n_clicks'),
     Input(component_id='dataset-dropdown', component_property='value')],
    [State(component_id='memory', component_property='data')])
def init_data(n_clicks, data_filename, data_dict):

    if n_clicks is None:
        raise PreventUpdate

    if data_filename in all_rnaseq_files:
        _, res = dbx.files_download(f'{rnaseq_files_path}/{data_filename}')
        with io.BytesIO(res.content) as stream:
            ts_df = pd.read_csv(stream, sep='\t', comment='#', index_col=0)
            ts_df.index.rename('symbol', inplace=True)
            pretty_name = data_filename.split('.')[0]
    else:
        _, res = dbx.files_download(f'{ma_files_path}/{data_filename}')
        with io.BytesIO(res.content) as stream:
            ts_df = pd.read_csv(stream, sep='\t', comment='#', index_col=3)
        ts_df = ts_df.drop(columns=['sgd_id', 'sys_name', 'probe'])
        pretty_name = '_'.join(data_filename.split('_')[:-6])

    timepoints = ts_df.columns.tolist()
    slider_dict = {str(idx): tp for idx, tp in zip(np.arange(0, len(timepoints), 1), timepoints)}
    total_genes = ts_df.shape[0]

    ts_df.reset_index(inplace=True)
    ts_json = ts_df.to_json()

    data_dict = {
        'pretty_name': pretty_name,
        'ts_data': ts_json,
        # 'jtk_data': jkt_df,
        'timepoints': timepoints,
        'slider_data': slider_dict,
        'total_genes': total_genes
    }

    return data_dict  # or, more generally, json.dumps(cleaned_df)


@app.callback(
    Output(component_id='heatmap', component_property='children'),
    Input(component_id='memory', component_property='modified_timestamp'),
    Input(component_id='period-slider', component_property='value'),
    Input(component_id='trunc-range-slider', component_property='value'),
    [State(component_id='memory', component_property='data'),
     State(component_id='heatmap', component_property='children')],
    prevent_initial_call=True
)
def update_heatmap_figure(ts, period, trunc_list, data_dict, children):

    if ts is None:
        raise PreventUpdate

    data_df = pd.read_json(data_dict['ts_data'], orient='columns')
    data_df = data_df.set_index('symbol')

    trunc_ts_df = data_df.iloc[:, trunc_list[0]:trunc_list[1]]

    return dcc.Graph(figure=heat.heatmap_max_periodicity(trunc_ts_df,
                                                         data_dict['pretty_name'],
                                                         data_df.index.tolist(), period),
                     config={'modeBarButtonsToRemove': ['autoScale2d', 'zoomOut2d', 'zoomIn2d', 'pan2d']}
                     )

@app.callback(
    Output(component_id='period-slider', component_property='min'),
    Output(component_id='period-slider', component_property='max'),
    Output(component_id='period-slider', component_property='value'),
    Input(component_id='memory', component_property='modified_timestamp'),
    [State(component_id='memory', component_property='data')],
    prevent_initial_call=True
)
def update_period_slider(ts, data_dict):

    if ts is None:
        raise PreventUpdate

    return 0, len(data_dict['timepoints'])-1,  3

@app.callback(
    Output(component_id='trunc-range-slider', component_property='min'),
    Output(component_id='trunc-range-slider', component_property='max'),
    Output(component_id='trunc-range-slider', component_property='value'),
    Output(component_id='trunc-range-slider', component_property='allowCross'),
    Input(component_id='memory', component_property='modified_timestamp'),
    [State(component_id='memory', component_property='data')],
    prevent_initial_call=True
)
def update_trunc_range_slider(ts, data_dict):

    if ts is None:
        raise PreventUpdate

    return 0, len(data_dict['timepoints'])-1, [0, len(data_dict['timepoints'])], False

@app.callback(
    Output(component_id='period-slider-output', component_property='children'),
    Input(component_id='period-slider', component_property='value'),
    Input(component_id='memory', component_property='modified_timestamp'),
    [State(component_id='memory', component_property='data')],
    prevent_initial_call=True
)
def update_period_slider_output(period_index, ts, data_dict):

    if ts is None:
        raise PreventUpdate

    return f'Select the time point with which the first cycle ends (Current selection {data_dict["timepoints"][period_index]}):'

@app.callback(
    Output(component_id='trunc-range-slider-output', component_property='children'),
    Input(component_id='trunc-range-slider', component_property='value'),
    Input(component_id='memory', component_property='modified_timestamp'),
    [State(component_id='memory', component_property='data')],
    prevent_initial_call=True
)
def update_trunc_range_slider_output(trunc_values, ts, data_dict):

    if ts is None:
        raise PreventUpdate

    left_idx = data_dict['timepoints'][trunc_values[0]]
    right_idx = data_dict['timepoints'][trunc_values[1]]
    return f'Select the range of time points to be displayed (Current selections: {left_idx} - {right_idx}):'

if __name__ == '__main__':
    app.run_server(debug=True)