import os
import json
import dash
import numpy as np
import pandas as pd
import plotly.express as px
import dash_cytoscape as cyto
import dash_core_components as dcc
import dash_html_components as html
import src.utilities.filters as fil
from dash.dependencies import Input, Output, State
import src.utilities.heatmap_functions as heat
import dash_bootstrap_components as dbc


app = dash.Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP], suppress_callback_exceptions=True)

RESULTS_DIR = '../../data/pipeline_results'

topdirs_list = [{'label': topdir, 'value': topdir} for topdir in os.listdir(RESULTS_DIR) if not topdir.startswith('.')]

app.layout = html.Div([
    dbc.Row([dbc.Col([
        dcc.Markdown('''
        ##### Select the dataset to be displayed:
        '''),
        dcc.Dropdown(
            id='dataset-dropdown',
            options=topdirs_list,
            placeholder="Select a dataset",
            style={'width': '500px'})
    ], width={'size': 4})
    ]),
    dbc.Row([
        dbc.Col([html.Div(children=[],
                          id='heatmap')],
                width={'size': 4, 'order': 'first'}),
        dbc.Col(html.Div(children=[],
                         id='network-col'),
                width={'size': 4}),
        dbc.Col([html.Div(children=[],
                         id='dsgrn-query-div'),
                html.Div(children=[],
                         id='dsgrn-query-network')],
                width={'size': 4})
    ])
])

@app.callback(
    Output(component_id='heatmap', component_property='children'),
    Input(component_id='dataset-dropdown', component_property='value'),
    prevent_initial_call=True
)
def update_heatmap_figure(data_dir):

    children = [dcc.Markdown('''
    ##### Gene expression of genes used for inferring local edges
    ''')]
    dir_dict = {'_'.join(pipe_dir.split('_')[:2]): pipe_dir for pipe_dir in os.listdir(f'{RESULTS_DIR}/{data_dir}') if os.path.isdir(f'{RESULTS_DIR}/{data_dir}/{pipe_dir}')}

    # get ts data and gene list
    if 'node_finding' in dir_dict.keys():
        config_file = fil.get_pipeline_step_config(f'{RESULTS_DIR}/{data_dir}', dir_dict['node_finding'])
        gene_list = fil.get_node_list(f'{RESULTS_DIR}/{data_dir}/{dir_dict["node_finding"]}/gene_list.tsv')
    else:
        config_file = fil.get_pipeline_step_config(f'{RESULTS_DIR}/{data_dir}', dir_dict['edge_finding'])
        gene_list_filename = os.path.split(config_file['gene_list_file'])[1]
        gene_list = fil.get_node_list(f'../../data/gene_lists/{gene_list_filename}')

    data_filenames = [os.path.split(filename)[1] for filename in config_file['data_files']]

    # numb_rows = fil.get_number_of_rows_nodes(data_filenames)

    for pidx, data_file in enumerate(data_filenames):
        pidx += 1
        ts_df = pd.read_csv(f'../../data/timeseries/{data_file}', comment='#', index_col=0, sep='\t')

        heatmap_fig = heat.heatmap_max_node(ts_df, data_file.split('.')[0], gene_list, round(len(ts_df.columns)/2))


        children.append(dcc.Graph(figure=heatmap_fig,
                                  config={'modeBarButtonsToRemove': ['autoScale2d', 'zoomOut2d', 'zoomIn2d', 'pan2d']}
                                  ))
    return children


@app.callback(
    Output(component_id='network-col', component_property='children'),
    Input(component_id='dataset-dropdown', component_property='value'),
    prevent_initial_call=True
)
def update_network_figure(data_dir):

    dir_dict = {'_'.join(pipe_dir.split('_')[:2]): pipe_dir for pipe_dir in os.listdir(f'{RESULTS_DIR}/{data_dir}') if os.path.isdir(f'{RESULTS_DIR}/{data_dir}/{pipe_dir}')}
    edge_config_file = fil.get_pipeline_step_config(f'{RESULTS_DIR}/{data_dir}', dir_dict['edge_finding'])
    data_filenames = [os.path.split(filename)[1] for filename in edge_config_file['data_files']]

    seed_config_file = fil.get_pipeline_step_config(f'{RESULTS_DIR}/{data_dir}', dir_dict['seed_network'])
    seed_filepath = f'{RESULTS_DIR}/{data_dir}/{dir_dict["seed_network"]}/{os.path.basename(seed_config_file["seed_net_file"])}'
    seed_elements, seed_styles = fil.make_cyto_seed_network_from_file(seed_filepath)

    sum_df = fil.get_lem_summaries(f'{RESULTS_DIR}/{data_dir}/{dir_dict["edge_finding"]}/summaries',
                                   len(data_filenames) - 1)

    net_children = [html.Div([dcc.Markdown('''##### Seed network for DSGRN network perturbations'''),
                cyto.Cytoscape(
                    layout={'name': 'circle'},
                    style={'height': '300px'},
                    elements=seed_elements,
                    stylesheet=seed_styles)
])]

    if seed_config_file['edge_score_column'] == 'pld':
        net_children.append(dcc.Graph(figure=px.histogram(sum_df, x="pld", title='Distribution of pld scores for local edges'),
                      config={'modeBarButtonsToRemove': ['autoScale2d', 'zoomOut2d', 'zoomIn2d', 'pan2d']},
                      style={'height': '350px', 'width': '500px'}
                      ))
    elif seed_config_file['edge_score_column'] == 'norm_loss':
        net_children.append(dcc.Graph(figure=px.histogram(sum_df, x="norm_loss", title='Distribution of normalized loss scores for local edges'),
                  config={'modeBarButtonsToRemove': ['autoScale2d', 'zoomOut2d', 'zoomIn2d', 'pan2d']},
                  style={'height': '350px', 'width': '500px'}
                  ))

    return net_children

@app.callback(
    Output(component_id='dsgrn-query-div', component_property='children'),
    Input(component_id='dataset-dropdown', component_property='value'),
    prevent_initial_call=True
)
def update_dsgrn_figure(data_dir):

    dir_dict = {'_'.join(pipe_dir.split('_')[:2]): pipe_dir for pipe_dir in os.listdir(f'{RESULTS_DIR}/{data_dir}') if os.path.isdir(f'{RESULTS_DIR}/{data_dir}/{pipe_dir}')}
    netque_config_file = fil.get_pipeline_step_config(f'{RESULTS_DIR}/{data_dir}', dir_dict['network_queries'])
    stablefc_results, domain_results = fil.get_netque_results(f'{RESULTS_DIR}/{data_dir}/{os.path.basename(dir_dict["network_queries"])}',
                                                              netque_config_file['stablefc'], netque_config_file['domain'])

    stablefc_df = fil.convert_stablefc_json_to_df(stablefc_results)
    stablefc_scatter = fil.dsgrn_scatter(stablefc_df, '% Matches', '% Stable Full Cycle', 0.01)

    dsgrn_children = [html.Div([dcc.Markdown('''##### DSGRN Network Perturbations and Queries'''),
        dcc.Graph(figure=stablefc_scatter, id='dsgrn-query-graph')])]

    return dsgrn_children


@app.callback(
    Output(component_id='dsgrn-query-network', component_property='children'),
    Input(component_id='dsgrn-query-graph', component_property='clickData'),
    prevent_initial_call=True
)
def update_dsgrn_query_network(clickData):

    network_topo = clickData['points'][0]['customdata'][0]
    # seed_elements, seed_styles = fil.make_cyto_seed_network(seed_filepath)
    net_elements, net_styles = fil.make_cyto_seed_network_from_string(network_topo)
    percent_matches = clickData['points'][0]['x']
    percent_stablefc = clickData['points'][0]['y']
    para_size_graph = clickData['points'][0]['customdata'][1]


    net_children = [html.Div([dcc.Markdown(f'''
                                           ##### DSGRN Predicted Network
                                           
                                           Parameter Size Graph: {para_size_graph}
                                           
                                           Percent Matches: {round(percent_matches, 2)}
                                           
                                           Percent Stable Full Cycles: {round(percent_stablefc, 2)}
                                           '''),
                              cyto.Cytoscape(
                                  layout={'name': 'circle'},
                                  style={'height': '300px'},
                                  elements=net_elements,
                                  stylesheet=net_styles)
                              ])]

    return net_children

if __name__ == '__main__':
    app.run_server(debug=True)