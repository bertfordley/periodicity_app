import pandas as pd
import streamlit as st
from matplotlib import pyplot as plt
import src.utilities.heatmap_functions as heat

st.set_option('deprecation.showPyplotGlobalUse', False)

st.title('Yeast Cell Cycle RNAseq')
st.write('(Ordered by max gene expression in first cycle)')


ts_df = pd.read_csv('../../data/processed/Scerevisiae_alphafactor_245-5min_RNAseq_dropped.tsv',
                    sep='\t', comment='#', index_col=0)

dl_df = pd.read_csv('../../data/periodicity_results/node_finding_190411114935/pydl_results.tsv',
                    sep='\t', comment='#', index_col=0)

jtk_df = pd.read_csv('../../data/periodicity_results/node_finding_190411114935/pyjtk_results.tsv',
                    sep='\t', comment='#', index_col=0)


# per_alg = st.sidebar.selectbox('Periodicity algorithm', ('pyJTK', 'pyDL'))
per_alg = st.sidebar.selectbox('Periodicity algorithm', ('pyJTK',))
thres = st.sidebar.text_input('Enter a p-value threshold')
numb_cols = ts_df.shape[1]
numb_genes = ts_df.shape[0]
cycle_col = st.sidebar.slider('First cycle column', 1, numb_cols)

front_trunc = st.sidebar.text_input('Enter front columns to drop')
back_trunc = st.sidebar.text_input('Enter back columns to drop')

period = ts_df.columns[cycle_col]
st.write(f'Period: {period}')

if front_trunc:
    ts_df = ts_df.iloc[:, int(front_trunc):]
if back_trunc:
    back_trunc = numb_cols - int(back_trunc)
    ts_df = ts_df.iloc[:, :back_trunc]



if per_alg == 'pyJTK':
    # plt.figure(figsize=(10,20))
    if thres:
        pyjtk_sorted = jtk_df.sort_values(by="p-value")
        pyjtk_sorted_thres = pyjtk_sorted[pyjtk_sorted["p-value"] <= float(thres)]
        percent_genes = round((pyjtk_sorted_thres.shape[0]/numb_genes) * 100, 2)
        gene_text = f'{pyjtk_sorted_thres.shape[0]} genes displayed ({percent_genes})% of all genes; p-value <= {thres}'
        st.write(gene_text)
        heat.heatmap_max(ts_df, pyjtk_sorted_thres.index.tolist(), cycle_col)
        st.pyplot()
    else:
        pyjtk_sorted = jtk_df.sort_values(by="p-value")
        pyjtk_sorted_thres = pyjtk_sorted[pyjtk_sorted["p-value"] <= 0.05]
        percent_genes = round((pyjtk_sorted_thres.shape[0]/numb_genes) * 100, 2)
        gene_text = f'{pyjtk_sorted_thres.shape[0]} genes displayed ({percent_genes})% of all genes'
        st.write(gene_text)
        heat.heatmap_max(ts_df, pyjtk_sorted_thres.index.tolist(), cycle_col)
        st.pyplot()

# elif per_alg == 'pyDL':
#     heat.plot_heatmap_max(ts_df, dl_df, per_alg.lower(), thres)
