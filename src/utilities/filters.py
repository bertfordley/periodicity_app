import os
import ast
import pandas as pd
from configobj import ConfigObj


# def pydl_filter_ts(ts_df, pydl_df, threshold, thres_rank=False):
#     # filters pydl df on dl_score column, or if thres_rank == True, filter by top N genes
#     pydl_sorted = pydl_df.sort_values(by="dl_score")
#     if thres_rank:
#         pydl_sorted_thres = pydl_sorted[:threshold]
#     else:
#         pydl_sorted_thres = pydl_sorted[pydl_sorted["dl_score"] <= threshold]
#     ts_filtered_df = ts_df[ts_df.index.isin(pydl_sorted_thres.index)]
#     return ts_filtered_df


def pyjtk_filter_ts(ts_df, pyjtk_df, threshold):
    # filters pyjtk df on p-value column, or if thres_rank == True, filter by top N genes
    pyjtk_sorted = pyjtk_df.sort_values(by="p-value")
    pyjtk_sorted_thres = pyjtk_sorted[pyjtk_sorted["p-value"] <= threshold]
    ts_filtered_df = ts_df[ts_df.index.isin(pyjtk_sorted_thres.index)]
    return ts_filtered_df

def get_node_list(gene_list_path):
    with open(gene_list_path, 'r') as f:
        node_list = []
        for line in f:
            if not line.startswith('#'):
                node_list.append(line.strip())
    return node_list[1:]

def get_pipeline_step_dir_or_file(dir_list, step_name):
    for pipeline_dir in os.listdir(dir_list):
        if step_name in pipeline_dir:
            return pipeline_dir

# parse config file
def parse_co(section,key):
    try:
        results = ast.literal_eval(section[key])
    except:
        results = section[key]
    return results

def get_pipeline_step_config(results_dir, pipeline_dir):

    config_file_name = get_pipeline_step_dir_or_file(f'{results_dir}/{pipeline_dir}', 'config')
    # config_file_name = os.path.basename(config_file)
    co = ConfigObj(ConfigObj(f'{results_dir}/{pipeline_dir}/{config_file_name}').walk(parse_co))
    return co

# def swap_gene_symbols(ts_df, node_list, swap_file_path):
#     swap_df = pd.read_csv(swap_file_path, sep='\t', index_col=1)
#     gs_ts_df = ts_df.merge(swap_df, left_index=True, right_index=True, how='outer')
#
#     gene_list = gs_ts_df[gs_ts_df.index.isin(node_list)]['TF'].tolist()
#
#     gs_ts_df = gs_ts_df.set_index('TF')
#
#     return gene_list, gs_ts_df

def make_cyto_seed_network(seed_net_filepath):
    cyto_elements = []
    cyto_styles = []
    cyto_styles.append({'selector': 'node', 'style': {'label': 'data(id)'}})
    cyto_styles.append({'selector': 'edge', 'style': {'curve-style': 'bezier'}})
    with open(seed_net_filepath, 'r') as f:
        for line in f:
            target, source, _ = line.split(' : ')
            source = source.strip('()')
            if source:
                cyto_elements.append({'data': {'id': source.strip('~'), 'label': source.strip('~')}})
                if '~' in source:
                    s = source.strip('~')
                    cyto_elements.append({'data': {'id': f'{s}{target}', 'source': s, 'target': target}})
                    cyto_styles.append({'selector': f'{s}{target}',
                                        'style': {'source-arrow-color': 'red', 'source-arrow-shape': 'rectangle',
                                                  'line-color': 'red'}})
                else:
                    cyto_elements.append({'data': {'id': f'{source}{target}', 'source': source, 'target': target}})
                    cyto_styles.append({'selector': f'{source}{target}',
                                        'style': {'source-arrow-color': 'green', 'source-arrow-shape': 'triangle',
                                                  'line-color': 'green'}})
            cyto_elements.append({'data': {'id': target, 'label': target}})
    cyto_elements = [i for n, i in enumerate(cyto_elements) if i not in cyto_elements[n + 1:]]
    return cyto_elements, cyto_styles


def get_lem_summaries(lempy_summaries_dirpath, numb_of_files):

    last_sum_df = pd.read_csv(f'{lempy_summaries_dirpath}/ts{numb_of_files}/allscores_ts{numb_of_files}.tsv',
                              comment='#', sep='\t', index_col=0)
    return last_sum_df

def get_number_of_rows_nodes(filename_list):

    if len(filename_list) % 3 > 0:
        numb_rows = int((len(filename_list) - (len(filename_list) % 3)) / 3) + 1
    else:
        numb_rows = int((len(filename_list)) / 3)
    return numb_rows