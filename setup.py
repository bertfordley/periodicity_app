from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='A webapp for periodicity analysis of time series datasets',
    author='bertfordley',
    license='MIT',
)
